FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9800

COPY target/*.jar instruction.jar

ENTRYPOINT [ "java","-jar","./instruction.jar"]
