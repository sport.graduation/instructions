package com.powrfit.instructions.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import com.powrfit.instructions.model.PlayerInstructions;
import com.powrfit.instructions.model.notificationModel;
import com.powrfit.instructions.repository.PlayerInstructionRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PlayerInstructionService {

    @Autowired
    PlayerInstructionRepository playerInstructionRepository;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Value("${playerNotification.rabbitmq.exchange}")
    private String exchangePlayerNotification;
    @Value("${playerNotification.rabbitmq.routingkey}")
    private String routingkeyPlayerNotification;

    @Transactional
    public ResponseEntity<?> addInstruction(PlayerInstructions instructionRequest) {
        playerInstructionRepository.save(instructionRequest);
        notificationModel notificationModel = new notificationModel(
            instructionRequest.getTitle(),
            instructionRequest.getContent()
        );
        sendInstruction(notificationModel);
        return ResponseEntity.ok("Instruction added successfully.");
    }

    public PlayerInstructions getInstruction(long instructionID, long player_ID) {
        Optional<PlayerInstructions> instruction = playerInstructionRepository.findById(instructionID);
        return instruction
            .orElseThrow(() -> new EntityNotFoundException("Instruction with id: " + instructionID + " Not found: "));
    }

    public List<PlayerInstructions> getPlayerInstructionList(long player_id) {
        return playerInstructionRepository.getInstructionsList(player_id);
    }

    public List<PlayerInstructions> getPlayerInstructionListByCategory(String category,long player_id){
        return playerInstructionRepository.getPlayerInstructionListByCategory(category,player_id);
    }

    public void setPlayerInstruction(boolean isDone, long player_id) {
        playerInstructionRepository.setPlayerInstruction(isDone, player_id);
    }

    void sendInstruction(notificationModel notificationModel){
        rabbitTemplate.convertAndSend(exchangePlayerNotification,routingkeyPlayerNotification, notificationModel);
    }
}
