package com.powrfit.instructions.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TeamInstruction {
    @Id
    @SequenceGenerator(
        name = "TeamInstruction_sequence",
        sequenceName = "TeamInstruction_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        generator = "TeamInstruction_sequence",
        strategy = GenerationType.SEQUENCE
    )
    @Column(name = "team_instruction_id", nullable = false)
    private Long teamInstruction_id;
    private long team_id;
    private String title;
    private String content;
    private String category;
}
