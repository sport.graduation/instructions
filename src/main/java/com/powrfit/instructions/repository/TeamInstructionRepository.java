package com.powrfit.instructions.repository;

import java.util.List;

import com.powrfit.instructions.model.TeamInstruction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TeamInstructionRepository extends JpaRepository<TeamInstruction,Long> {
    @Query(value = "SELECT * FROM team_instruction u where u.category = :category and u.team_id = :team_id",
           nativeQuery = true)
    List<TeamInstruction> getTeamInstructionListByCategory(String category, long team_id);

    @Query(value = "SELECT * FROM team_instruction u where u.team_id = :team_id", nativeQuery = true)
    List<TeamInstruction> getTeamInstructionList(long team_id);
}
