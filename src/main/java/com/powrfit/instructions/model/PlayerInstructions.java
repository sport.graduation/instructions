package com.powrfit.instructions.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PlayerInstructions implements Serializable {
    @Id
    @SequenceGenerator(
        name = "PlayerInstructions_sequence",
        sequenceName = "PlayerInstructions_sequence",
        allocationSize = 1
    )
    @GeneratedValue(
        generator = "PlayerInstructions_sequence",
        strategy = GenerationType.SEQUENCE
    )
    @Column(name = "instruction_id", nullable = false)
    private long instruction_id;
    private String content;
    private String title;
    private String category;
    private boolean isDone = false;
    private long player_id;
    private long coach_id;

}
