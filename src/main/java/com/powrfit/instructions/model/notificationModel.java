package com.powrfit.instructions.model;

import java.io.Serializable;

import lombok.ToString;

@lombok.Data
@ToString
public class notificationModel implements Serializable {
    private String title;
    private String body;


    public notificationModel(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
