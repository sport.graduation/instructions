package com.powrfit.instructions.service;

import java.util.List;

import javax.transaction.Transactional;

import com.powrfit.instructions.model.PlayerInstructions;
import com.powrfit.instructions.model.TeamInstruction;
import com.powrfit.instructions.model.notificationModel;
import com.powrfit.instructions.repository.TeamInstructionRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TeamInstructionService {
    @Autowired
    private TeamInstructionRepository teamInstructionRepository;

    @Autowired
    RabbitTemplate rabbitTemplate;
    @Value("${teamNotification.rabbitmq.exchange}")
    private String exchangeTeamNotification;
    @Value("${teamNotification.rabbitmq.routingkey}")
    private String routingkeyTeamNotification;

    public List<TeamInstruction> getTeamInstructionList(long team_id) {
        return teamInstructionRepository.getTeamInstructionList(team_id);
    }
    @Transactional
    public ResponseEntity<?> addInstruction(TeamInstruction requestTeamInstruction) {
        teamInstructionRepository.save(requestTeamInstruction);

        notificationModel notificationModel = new notificationModel(
            requestTeamInstruction.getTitle(),
            requestTeamInstruction.getContent()
        );
        sendInstruction(notificationModel);
        return ResponseEntity.ok("Instruction added successfully.");
    }

    public List<TeamInstruction> getTeamInstructionListByCategory(String category, long team_id) {
        return teamInstructionRepository.getTeamInstructionListByCategory(category,team_id);
    }
    @Transactional
    public ResponseEntity<?> deleteTeamInstruction(long instruction_id) {
         teamInstructionRepository.deleteById(instruction_id);
        return ResponseEntity.ok("Instruction deleted successfully.");
    }

    void sendInstruction(notificationModel notificationModel){
        rabbitTemplate.convertAndSend(exchangeTeamNotification,routingkeyTeamNotification, notificationModel);
    }
}
