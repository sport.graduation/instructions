package com.powrfit.instructions.controller;

import java.util.List;

import com.powrfit.instructions.model.PlayerInstructions;
import com.powrfit.instructions.model.TeamInstruction;
import com.powrfit.instructions.service.TeamInstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/instructions/team")
public class TeamInstructionController {

    @Autowired
    private TeamInstructionService teamInstructionService;

    @RequestMapping(value = "/all/{team_id}", method = RequestMethod.GET)
    ResponseEntity<?> GetTeamInstructionsList(@PathVariable long team_id) {
        return ResponseEntity.status(HttpStatus.OK).body(teamInstructionService.getTeamInstructionList(team_id));
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<?> GetTeamInstructions(@RequestBody TeamInstruction requestTeamInstruction) {
        teamInstructionService.addInstruction(requestTeamInstruction);
        return ResponseEntity.ok("Team Instruction is add successful");
    }

    @RequestMapping(value = "/{team_id}", method = RequestMethod.GET)
    ResponseEntity<?> GetTeamInstructionsListByCategory(@RequestParam String category,
                                                                 @PathVariable long team_id) {
        return ResponseEntity.status(HttpStatus.OK).body(teamInstructionService.getTeamInstructionListByCategory(category, team_id));
    }

    @RequestMapping(value = "/{instruction_id}", method = RequestMethod.DELETE)
    ResponseEntity<?> GetTeamInstructionsListByCategory(@PathVariable long instruction_id) {
        return teamInstructionService.deleteTeamInstruction(instruction_id);
    }

}
