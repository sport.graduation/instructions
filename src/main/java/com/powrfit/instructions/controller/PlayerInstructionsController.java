package com.powrfit.instructions.controller;

import java.util.List;

import com.powrfit.instructions.model.PlayerInstructions;
import com.powrfit.instructions.service.PlayerInstructionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/instructions/player")
public class PlayerInstructionsController {

    @Autowired
    private PlayerInstructionService playerInstructionService;

    @RequestMapping(value = "/all/{player_id}", method = RequestMethod.GET)
    ResponseEntity<?> GetPlayerInstructionsList(@PathVariable long player_id) {
        log.info("Get Player Instruction list");
        return ResponseEntity.status(HttpStatus.OK).body(playerInstructionService.getPlayerInstructionList(player_id));
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<?> AddPlayerInstructions(@RequestBody PlayerInstructions requestPlayerInstructions) {
        playerInstructionService.addInstruction(requestPlayerInstructions);
        return ResponseEntity.ok("Player Instruction is add successful");
    }

    @RequestMapping(value = "/{player_id}", method = RequestMethod.GET)
    ResponseEntity<?> GetPlayerInstructionsListByCategory(@RequestParam String category,
                                                                 @PathVariable long player_id) {
        return ResponseEntity.status(HttpStatus.OK).body(playerInstructionService.getPlayerInstructionListByCategory(category, player_id));
    }

    @RequestMapping(value = "/setInstruction/{player_id}", method = RequestMethod.PUT)
    ResponseEntity<?> SetPlayerInstructionDone(@RequestParam boolean isDone, @PathVariable long player_id) {
        playerInstructionService.setPlayerInstruction(isDone, player_id);
        return ResponseEntity.ok("Player Instruction is set Done successful");
    }
}
