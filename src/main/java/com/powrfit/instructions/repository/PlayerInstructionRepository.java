package com.powrfit.instructions.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.powrfit.instructions.model.PlayerInstructions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerInstructionRepository extends JpaRepository<PlayerInstructions, Long> {

    @Query(value = "SELECT * FROM player_instructions u where u.player_id = :player_id", nativeQuery = true)
    List<PlayerInstructions> getInstructionsList(long player_id);

    @Query(value = "SELECT * FROM player_instructions u where u.category = :category and u.player_id = :player_id",
           nativeQuery = true)
    List<PlayerInstructions> getPlayerInstructionListByCategory(String category, long player_id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE player_instructions u SET u.is_done = :isDone where u.player_id = :player_id",
           nativeQuery = true)
    void setPlayerInstruction(boolean isDone, long player_id);
}
